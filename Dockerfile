ARG TELEGRAF_TAG=1.22.1
FROM telegraf:${TELEGRAF_TAG}

RUN export DEBIAN_FRONTEND=noninteractive && \
    export DEBIAN_RELEASE=$(awk -F'[" ]' '/VERSION=/{print $3}'  /etc/os-release | tr -cd '[[:alnum:]]._-' ) && \
    sed -i -e's/ main/ main contrib non-free/g' /etc/apt/sources.list && \
    set -x && \
    apt-get update && \
    apt-get -y install snmp snmp-mibs-downloader && \
    rm -r /var/lib/apt/lists/*

RUN mkdir -p $HOME/.snmp/mibs
RUN sed -i '/mibs/d' /etc/snmp/snmp.conf
RUN echo "mibs +ALL" >> /etc/snmp/snmp.conf

RUN /usr/bin/download-mibs
COPY mibs/synology /usr/share/snmp/mibs
COPY mibs/SNMPv2-PDU-MIB.txt /usr/share/snmp/mibs/ietf/SNMPv2-PDU

USER telegraf
